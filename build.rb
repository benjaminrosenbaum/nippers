#!/usr/bin/env ruby

title = ARGV[0] || "story"

research = File.exist?("research")

unless File.exist?("target")
	`mkdir target`
end

unless File.file?("./nippers.rb") 
	if File.file?("../nippers/nippers.rb")
		`ln -s ../nippers/nippers.rb .`
	else
		`curl https://bitbucket.org/benjaminrosenbaum/nippers/raw/master/nippers.rb > nippers.rb; chmod a+x nippers.rb`
	end
end

compile = "./nippers.rb --compile --with-headers 3 "

`#{compile} src | pandoc -s -o "target/#{title}.html"`
`#{compile} src | pandoc -s -o "target/story.txt"`
`#{compile} research | pandoc -s --toc -o "target/#{title}-research.html"` if research
`open target/#{title}-research.html` if research
`open target/#{title}.html`

if ARGV[0]
	`#{compile} --indent 2 src | pandoc -s -o "target/#{title}.rtf" `
	`#{compile} -s "h3 { font-weight: normal; text-transform: uppercase; font-size: inherit}" --manuscript-format --title "#{title}" src | pandoc -s -o "target/#{title}.with.headers.ms.html"`
	`./nippers.rb -c -i 2 -m src | pandoc -s -o "target/#{title}.epub" `
	`./nippers.rb --compile --manuscript-format --title "#{title}" src | pandoc -s -o "target/#{title}.ms.html"`
	`echo "<a name='END'></a><script>document.location.href='#END'</script>" >> target/#{title}.with.headers.ms.html`
	`open target/#{title}.with.headers.ms.html`

	
	#TODO PDF
end