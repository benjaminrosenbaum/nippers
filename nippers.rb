#!/usr/bin/env ruby

require 'optparse'


def log(msg)
	$stderr.print "#{msg}\n" if @options[:verbose]
end


# build a map from canonical names of files to their current expressed names
def build_file_map(target_dir)
	files = {}
	Dir.foreach(target_dir) do |item|
		if item =~ /([0-9]* )?(.*)\.md/ 
			log "found item #{$2}.md"
			files["#{$2}.md"] = item  # files is an index of canonical name to found name
		end
	end
	files
end

def order_file(src, dst, target_dir)
	unless @options[:no_reorder] 
		destination = File.join(target_dir, dst)
		if src
			source = File.join(target_dir, src)
			log "renaming [#{source}] \t as [#{destination}]\n"
			File.rename(source, destination) 
		else
			log "creating new file [#{destination}]"
			`touch "#{destination}"`
		end
	end
end 

def reorder_files(files_list_path, target_dir, file_map)
	l = 0
	new_file_map = {}
	if File.file?(files_list_path)
		File.open(files_list_path) do |file|
			log "opened #{files_list_path}"
			file.each do |line|
				line = line.chomp
				log "---> got #{line}"
				dst = "%03d #{line}" % l
				if not line.empty?
					if file_map[line]
						    log " ------> which corresponds to #{file_map[line]}"
						    order_file(file_map[line], dst, target_dir)
					else
							log "-----> no file found matching #{line}"
							order_file(nil, dst, target_dir)	
					end 
					l += 1 
					new_file_map[line] = dst
				end
			end
		end
		(file_map.keys - new_file_map.keys).each do |unused|
			log "[#{file_map[unused]}] missing from files.txt) \n"
			order_file(file_map[unused], unused, target_dir)
		end
	else
		fail "Cannot find #{files_list_path}!"
	end
	new_file_map
end

def compile(file_map, source_dir)
	lines = []
	words = 0
	chapter_num = 0
	file_map.each do |name, path|
		if path
			chapter_num = chapter_num + 1
			if @options[:manuscript]
				lines << "<div style='text-align: center'><br/>&#x23;<br/><br/></div>" if chapter_num > 1
			end
			if level = @options[:with_headers] 
				title = case @options[:header_format]
				when :number
					chapter_num	
				else
					File.basename(name, ".*")
				end
				log "---> got #{title} from #{name} at #{level}"
				lines << "%s #{title}\n" % ("#" * level.to_i) 
			end
			filename = File.join(source_dir, path)
			log "---> Reading #{filename}" 
			lines << "%s\n\n" % IO.read(filename)
			words += `wc -w "#{filename}" | awk '{print $1}'`.to_i
		end
	end

	if @options[:indent] || @options[:manuscript]
		print """<style>
p {
    text-indent:#{@options[:indent] || 2}em;
    margin: 0;
}
div p {
	line-height: 1em; 
	text-indent: 0em;
}
#{ @options[:additional_styling] }
</style>""" 	
	end
	if @options[:manuscript]

	#since we don't want to check contact info into a public repo, put it in a local "contact.info" file
	contact = IO.read('./contact.info').split("\n") rescue ['AUTHOR NAME', 'STREET', 'CITY, STATE ZIP', '', 'PHONE']
	author = contact.shift  
	address = contact 

		print """<style>
body { font-family: 'Courier New'; line-height: 2em; }
</style>
<p><br/><br/><br/></p>
<div id='front-matter'  >
#{author}#{"&nbsp;" * 20 }About #{ words / 100 * 100} words<br/>
#{ address.join('<br/>') }
</div>
<p><br/><br/><br/><br/><br/><br/></p>
<div style='clear:both; text-align: center'>
#{@options[:title] || 'TITLE_GOES_HERE' }
<br/><br/>
by #{author}
<br/>
</div>
<p><br/><br/><br/><br/><br/><br/></p>
"""
	end
	print lines.join("\n")
end

@options = {}
optparser = OptionParser.new do |opts|
  opts.banner = "Usage: #{$0} [<options>] <project target_directory>\n\nOptions:"
  opts.on("-v", "--[no-]verbose", "Run verbosely") { |v| @options[:verbose] = v } 
  opts.on("-i [EMs]", "--indent [EMs]", "Indent EMs and remove paragraph margins") {|ems| @options[:indent] = ems }
  opts.on("-t [title]", "--title [title]", "Story title for manuscript format") {|title| @options[:title] = title }
  opts.on("--no-reorder", "Suppress renaming of project files according to order in files.txt") { |v| @options[:no_reorder] = v } 
  opts.on("-c", "--compile", "Compile in files.txt order, with separating lines, suitable for streaming to pandoc") { |v| @options[:compile] = v}
  opts.on("-w [LEVEL]", "--with-headers [LEVEL]", "When compiling, add filenames as level LEVEL headers") do |level| 
  	@options[:with_headers] = level || 2
  end
  opts.on("-f", "--header-format [FORMAT]", [:title, :number], "When compiling with headers, format of header (title, number)") do |format|
  	@options[:header_format] = format || :title
  end

  opts.on("-s", "--additional-styling [CSS]", "Additional styling in CSS for output") do |css|
  	@options[:additional_styling] = css || ''
  end
  opts.on("-h", "--help", "Prints this help") do
  	puts opts
    exit
  end
  opts.on("-m", "--manuscript-format") { |v| @options[:manuscript] = v }
end
optparser.parse!

unless ARGV[0] 
	print "\nMissing target project directory.\n"
	puts optparser
	exit
end


target_dir = File.expand_path(ARGV[0])
log ">>> Source target_directory: #{target_dir}"
files_list_path = File.join(target_dir, "files.txt")
file_map = build_file_map(target_dir)
new_file_map = reorder_files(files_list_path, target_dir, file_map) 
compile(new_file_map, target_dir) if @options[:compile]

