#!/usr/bin/env ruby

require 'fileutils'
require 'test/unit'



class TestNippers < Test::Unit::TestCase

	def setup
		@base_dir = File.expand_path(File.join(File.dirname(__FILE__), ".."))
		@test_src_dir = File.join(@base_dir, "test/test-src")
		@orig_src_dir = File.join(@base_dir, "test/orig-src/.")
		@executable = File.join(@base_dir, "nippers.rb")

		print "Prepare target directory #{@test_src_dir}\n"
		exec_cmd("rm -rf #{@test_src_dir}")
		exec_cmd("cp -r #{@orig_src_dir} #{@test_src_dir}")

#		FileUtils.rm_rf(@test_src_dir)
#		FileUtils.cp_r(@orig_src_dir, @test_src_dir)
	end

	def test_torah_order
		check_order("torah", """000 In the Beginning.md
001 Names.md
002 And he called.md
003 In the Desert.md
004 Laws.md
Notes.md
files.txt
""")
	end

	def test_length_order
		check_order("length", """000 Laws.md
001 Names.md
002 In the Desert.md
003 And he called.md
004 In the Beginning.md
Notes.md
files.txt
""")
	end


	def test_alphabetical_order
		check_order("alphabetical", """000 And he called.md
001 In the Beginning.md
002 In the Desert.md
003 Laws.md
004 Names.md
Notes.md
files.txt
""")
	end

	def test_single_order_and_removal
		check_order("single", """000 Laws.md
001 Names.md
002 Notes.md
And he called.md
In the Beginning.md
In the Desert.md
files.txt
""")
	end

	def test_new_file_created_from_list
		print "\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n"
		check_order("additional", """000 In the Beginning.md
001 Names.md
002 And he called.md
003 In the Desert.md
004 Laws.md
005 Joshua.md
006 Judges.md
Notes.md
files.txt
""")

	end



	def test_compile_with_title_headers
		set_ordering("torah")
		assert_equal("""### In the Beginning

Let there be light.

### Names

\"_I am what I am._\"

\\# 

Let my people *go*.

### And he called

Remember the Sabbath and keep it holy.

### In the Desert

Miriam's hand turned white.

### Laws

Observe the Sabbath and keep it holy.

""", `#{@executable} -c -w 3 #{@test_src_dir}`)
	end


	def set_ordering(orderName)
		orderSrc = File.join(@base_dir, "test/#{orderName}.files.txt")
		orderDest = File.join(@test_src_dir, "files.txt")
		exec_cmd("cp #{orderSrc} #{orderDest}")
	end

	def check_order(orderName, expectedOrder)
		set_ordering(orderName)
		ls = exec_cmd("#{@executable} #{@test_src_dir}; ls -1 #{@test_src_dir} | sort")
		assert_equal(expectedOrder.gsub(/\s+/, ' '), ls.gsub(/\s+/, ' '), "#{expectedOrder} order should match")
	end

	def exec_cmd(cmd)
		print "Executing #{cmd}\n"
		`#{cmd}`
	end


end