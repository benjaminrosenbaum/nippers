# Nippers #

Nippers is an attempt to create a git-friendly writing platform. The thought is to base it on Pandoc and initially Sublime, possibly heading towards Atom.

Nippers was the name of one of the scriveners who worked alongside Bartelby in Melville's story.

### How do I get set up? ###

1) Make a new directory for your story and put it under source control:
 
```  
 mkdir MyStory  
 cd MyStory  
 git init  
```

2) Create a "src" subdirectory to hold your chapters as markdown files:
 
```
 mkdir src  
 cat "A chapter about something" > src/"Something.md"  
 cat "Another chapter, *wow!*" > src/"Oh Wow.md"  
 cat "Some notes" > src/Notes.md  
```

3) Create a "src/files.txt" file that captures the order of the files you want to include. It should look like this:
 
```
 Something.md
 Oh Wow.md  
```

   Whenever you run nippers, the files will be reordered to correspond to the order in this file. If you add a new line to this file, a blank file with that title will be created. So you use this file as the "directory" to organize your chapters.

   Nippers adds numeric prefixes when it orders the files, like "000 Something.md". Ignore these prefixes; don't include them in files.txt.

4) Install [Pandoc](http://pandoc.org/installing.html) 

5) Install Nippers 
 
```
 curl https://bitbucket.org/benjaminrosenbaum/nippers/raw/master/nippers.rb > nippers.rb  
 chmod a+x nippers.rb  
```

6) Compile to your desired formats
 
```
 ./nippers.rb -c -w 3 -f title src | pandoc -o MyStory.rtf -s  
 ./nippers.rb -c -w 3 -f title src | pandoc -o MyStory.html -s  
 ./nippers.rb -c -w 3 -f title src | pandoc -o MyStory.epub -s  	
```

7) Install as a build system in Sublime
 [instructions TODO]


